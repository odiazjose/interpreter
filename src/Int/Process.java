package Int;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Process implements Runnable{
    private Thread thread;
    private Matcher analizer;
    private JFrame outputWindow;
    private DefaultTableModel outputTableModel, outputTableModel2, outputTableModel3;
    private final Pattern[] validPatterns, validInstSeq;
    private final String[] tokenNames, validInstSeqMsg;
    private String[] instructionLines;
    private ArrayList<String> variables;
    private ArrayList varValue;
    
    Process(){
        variables = new ArrayList<>();
        varValue = new ArrayList<>();
        
        Integer patterns = 8;
        tokenNames = new String[patterns];
        validPatterns = new Pattern[patterns];
        
        tokenNames[0] = "Reserved word";
        validPatterns[0] = Pattern.compile("var|function|if|else|do|for|while|return|true|false|break|continue|"
                + "int|float|double|string|integer|boolean|char|class|public|private|abstract");                   
        
        tokenNames[1] = "Variable name";
        validPatterns[1] = Pattern.compile("^[_a-zA-Z]\\w*$");                                               
        
        tokenNames[2] = "Constant";
        validPatterns[2] = Pattern.compile("-?\\d+\\.?\\d*");                                                  
        
        tokenNames[3] = "Character string";
        validPatterns[3] = Pattern.compile("^\"\\s*[^\"]*\"$");                                             
        
        tokenNames[4] = "Special character";
        validPatterns[4] = Pattern.compile("^(={1,3}|\\|\\.|,|;|!|/|\\(|\\)|\\[|\\]|\\{|\\}|-|\\+|\\*)");       
        
        tokenNames[5] = "Function declaration";
        validPatterns[5] = Pattern.compile("^[_a-zA-Z]\\d*\\(\\)\\{.*\\}$");                                     
        
        tokenNames[6] = "Function call";
        validPatterns[6] = Pattern.compile("(if|for|while|^[_a-zA-Z]\\w*)\\((\\w*|={1,3})*\\)$");                
        
        tokenNames[7] = "Assignment";
        validPatterns[7] = Pattern.compile("^[_a-zA-Z]\\w*=(-?\\d*\\.?\\d*|[_a-zA-Z]\\w*|\".*\")");                
        
        Integer instructionSequences = 15;
        validInstSeqMsg = new String[instructionSequences];
        validInstSeq = new Pattern[instructionSequences];
        
        validInstSeqMsg[0] = "Variable declaration";
        validInstSeq[0] = Pattern.compile("01+");
        
        validInstSeqMsg[1] = "Variable declaration and constant assignment";
        validInstSeq[1] = Pattern.compile("0123");
        
        validInstSeqMsg[2] = "Constant assignment";
        validInstSeq[2] = Pattern.compile("123");
        
        validInstSeqMsg[3] = "Variable declaration and string assignment";
        validInstSeq[3] = Pattern.compile("0124");
        
        validInstSeqMsg[4] = "String assignment";
        validInstSeq[4] = Pattern.compile("124");
        
        validInstSeqMsg[5] = "Variable declaration and constant assignment with operation";
        validInstSeq[5] = Pattern.compile("0123((A|B|C|D)(1|3))*");
        
        validInstSeqMsg[6] = "Constant assignment with operation";
        validInstSeq[6] = Pattern.compile("123((A|B|C|D)(1|3))*");
        
        validInstSeqMsg[7] = "Variable declaration and string assignment with concatenation";
        validInstSeq[7] = Pattern.compile("0124(A(1|3|4))*");
        
        validInstSeqMsg[8] = "String assignment with concatenation";
        validInstSeq[8] = Pattern.compile("124(A(1|3|4))*");
        
        validInstSeqMsg[9] = "Variable declaration with variable assignment";
        validInstSeq[9] = Pattern.compile("0121");
        
        validInstSeqMsg[10] = "Variable assignment";
        validInstSeq[10] = Pattern.compile("121");
        
        validInstSeqMsg[11] = "Variable declaration with variable assignment and operation";
        validInstSeq[11] = Pattern.compile("0121((A|B|C|D)(1|3|4))*");
        
        validInstSeqMsg[12] = "Variable assignment and operation";
        validInstSeq[12] = Pattern.compile("121((A|B|C|D)(1|3|4))*");
        
        validInstSeqMsg[13] = "Variable declaration and boolean assignment";
        validInstSeq[13] = Pattern.compile("0125");
        
        validInstSeqMsg[14] = "Boolean assignment";
        validInstSeq[14] = Pattern.compile("125");
    }
    
    private void symbolAnalisisOutput(String token, String value){
        outputTableModel.addRow(new Object[]{token, value});
    }
    
    private void instructionSequenceAnalisisOutput(String instruction, String action, String operation){
        outputTableModel2.addRow(new Object[]{instruction, action, operation});
    }
    
    private void variableOutput(String variableName, String value){
        outputTableModel3.addRow(new Object[]{variableName, value});
    }

    @Override
    public void run() {
        boolean matchFound, assignment;
        String[] words;
        String analizedInstSeq, analizedInstOpe;
        ArrayList<Character> operationSeq = new ArrayList<>();
        ArrayList operands = new ArrayList<>();
        
        variables.clear();
        varValue.clear();
            
        for(String instruction : instructionLines){
            analizedInstSeq = "";
            analizedInstOpe = "";
            operationSeq.clear();
            operands.clear();
            assignment = false;
            words = instruction.split(" |,");
            
            for(String word : words){
                word = word.replaceAll("\\s", "");
                if(word.length() == 0) continue;
                if(word.matches("(.(\\+|-|/|\\*).)*")) System.out.println(word);
                matchFound = false;
                
                for(int token = 0; (token < validPatterns.length) && (!matchFound); token++){
                    analizer = validPatterns[token].matcher(word);
                    
                    if(analizer.matches()){
                        switch(token){
                            case 0:
                                matchFound = true;
                                if(word.matches("var")) analizedInstSeq += "0";
                                if(word.matches("true")) analizedInstSeq += "5";
                                if(word.matches("false")) analizedInstSeq += "5";
                                break;
                            case 1:
                                matchFound = true;
                                analizedInstSeq += "1";
                                System.out.println("Inst seq: " + analizedInstSeq);
                                if(analizedInstSeq.matches("01+")){
                                    variables.add(word);
                                    varValue.add("Undefined");
                                    operands.add(word);
                                } else operands.add(word);
                                break;
                            case 2:
                                matchFound = true;
                                analizedInstSeq += "3";
                                operands.add(word);
                                break;
                            case 3:
                                matchFound = true;
                                analizedInstSeq += "4";
                                break;
                            case 4:
                                matchFound = true;
                                if(word.matches("=")){
                                    analizedInstSeq += "2";
                                    assignment = true;
                                }
                                if(word.matches("\\+")){
                                    analizedInstSeq += "A";
                                    analizedInstOpe += "Addition. ";
                                    operationSeq.add('A');
                                }
                                if(word.matches("-")){
                                    analizedInstSeq += "B";
                                    analizedInstOpe += "Substraction. ";
                                    operationSeq.add('B');
                                }
                                if(word.matches("\\*")){
                                    analizedInstSeq += "C";
                                    analizedInstOpe += "Multiplication. ";
                                    operationSeq.add('C');
                                }
                                if(word.matches("/")){
                                    analizedInstSeq += "D";
                                    analizedInstOpe += "Division. ";
                                    operationSeq.add('D');
                                }
                                break;
                            case 5:
                                matchFound = true;
                                break;
                            case 6:
                                matchFound = true;
                                break;
                            case 7:
                                matchFound = true;
                                if(word.matches("^[_a-zA-Z]\\w*=-?\\d*\\.?\\d*")) analizedInstSeq += "123";
                                else analizedInstSeq += "124";
                                break;
                        }
                        
                        symbolAnalisisOutput(tokenNames[token], word);
                    }
                }
                if(!matchFound){
                    symbolAnalisisOutput("Unknown token", word);
                    
                    JOptionPane.showMessageDialog(outputWindow, "Error: Unknown token " + word, "Unknown token", 0);
                    return;
                }
            }
            
            Boolean sequenceFound = false;
            for(Integer i = 0; (i < validInstSeq.length) && (!sequenceFound); i++){
                analizer = validInstSeq[i].matcher(analizedInstSeq);
                if(analizer.matches()){
                    sequenceFound = true;
                    instructionSequenceAnalisisOutput(instruction, validInstSeqMsg[i], analizedInstOpe);
                    
                    Boolean undeclaredVar = false;
                    
                    Object targetVar = operands.get(0);
                    operands.remove(targetVar);
                    
                    Integer currentVarValue = (Integer) searchVar(targetVar.toString())[1];
                    if(currentVarValue == null) undeclaredVar = true;
                    
                    //High priority operations
                    for(Integer j = 0; j < operationSeq.size() && !undeclaredVar; j++){
                        Character c = operationSeq.get(j);
                        if(c == 'C' || c == 'D'){
                            Object op1 = operands.get(j);
                            Object op2 = operands.get(j + 1);
                            if(op1.toString().matches(validPatterns[1].pattern())){
                                op1 = searchVar(op1.toString())[0];
                                if(op1 == null) undeclaredVar = true;
                            }
                            if(op2.toString().matches(validPatterns[1].pattern())){
                                op2 = searchVar(op2.toString())[0];
                                if(op2 == null) undeclaredVar = true;
                            }
                            if(!undeclaredVar){
                                if(c == 'C') varValue.set(currentVarValue, Float.parseFloat(op1.toString()) * Float.parseFloat(op2.toString()));
                                else varValue.set(currentVarValue, Float.parseFloat(op1.toString()) / Float.parseFloat(op2.toString()));
                                operands.set(j, varValue.get(currentVarValue));
                                operands.remove(j + 1);
                                operationSeq.remove(c);
                                j--;
                            }
                        }
                    }
                    
                    //Low priority operations
                    for(Integer j = 0; j < operationSeq.size() && !undeclaredVar; j++){
                        Character c = operationSeq.get(j);
                        if(c == 'A' || c == 'B'){
                            Object op1 = operands.get(j);
                            Object op2 = operands.get(j + 1);
                            if(op1.toString().matches(validPatterns[1].pattern())){
                                op1 = searchVar(op1.toString())[0];
                                if(op1 == null) undeclaredVar = true;
                            }
                            if(op2.toString().matches(validPatterns[1].pattern())){
                                op2 = searchVar(op2.toString())[0];
                                if(op2 == null) undeclaredVar = true;
                            }
                            if(!undeclaredVar){
                                if(c == 'A') varValue.set(currentVarValue, Float.parseFloat(op1.toString()) + Float.parseFloat(op2.toString()));
                                else varValue.set(currentVarValue, Float.parseFloat(op1.toString()) - Float.parseFloat(op2.toString()));
                                operands.set(j, varValue.get(currentVarValue));
                                operands.remove(j + 1);
                                operationSeq.remove(c);
                                j--;
                            }
                        }
                    }
                    
                    //Simple assignment
                    if(operands.size() > 0){
                        Object operand = operands.get(0);
                        if(operand.toString().matches(validPatterns[1].pattern())){
                            operand = searchVar(operand.toString())[0];
                            if(operand == null) undeclaredVar = true;
                        }
                        if(!undeclaredVar) varValue.set(currentVarValue, operand);
                    }
                }
            }
            
            if(!sequenceFound){
                instructionSequenceAnalisisOutput(instruction, "Unkown instruction sequence", "");
                JOptionPane.showMessageDialog(outputWindow, "Error: Unknown instruction sequence: " + instruction, "Unknown instruction sequence", 0);
                return;
            }
        }
        
        checkList(variables, varValue);
        
        
        
        System.out.println("\nOperands:");
        Iterator i = operands.iterator();
        while(i.hasNext()){
            System.out.println(i.next());
        }
        
        for(Integer j = 0; j < variables.size(); j++){
            variableOutput(variables.get(j), varValue.get(j).toString());
        }
    }
    
    private Object[] searchVar(String varName){
        Object[] var = new Object[2];
        
        for(Integer i = 0; i < variables.size(); i++)
            if(variables.get(i).equals(varName)){
                var[0] = varValue.get(i);
                var[1] = i;
                break;
            }
        
        return var;
    }
    
    public void start(String[] instructions){
        if(thread == null || !thread.isAlive()){
            thread = new Thread(this);
        
            instructionLines = instructions;

            thread.start();
        }
    }
    
    public void setOutput(DefaultTableModel outputTableModel_1, DefaultTableModel outputTableModel_2, DefaultTableModel outputTableModel_3, JFrame parent){
        outputWindow = parent;
        outputTableModel = outputTableModel_1;
        outputTableModel2 = outputTableModel_2;
        outputTableModel3 = outputTableModel_3;
    }
    
    private void checkList(ArrayList<String> variable, ArrayList<Float> value){
        Iterator<String> iterVar = variable.iterator();
        Iterator<Float> iterVal = value.iterator();
        
        System.out.println("\nVariables:");
        while(iterVar.hasNext()){
            System.out.println(iterVar.next());
        }
        
        System.out.println("\nValues:");
        while(iterVal.hasNext()){
            System.out.println(iterVal.next());
        }
    }
}